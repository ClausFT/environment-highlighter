var sites = 
[
    // ---------------------- LOCAL ----------------------
    {
        "name" : "boc.sitecore.website.local",
        "environment" : "local"
    },
	{
        "name" : "molio.moliodk.local",
        "environment" : "local"
    },

    // ---------------------- DEV ----------------------
    {
        "name" : "test-boc.boconcept.com",
        "environment" : "dev"
    },
	{
        "name" : "dev.molio.dk",
        "environment" : "dev"
    },

    // ---------------------- TEST ----------------------
    {
        "name" : "edit-preprod-boc.boconcept.com",
        "environment" : "test"
    },

    // ---------------------- PROD ----------------------
    {
        "name" : "edit-prod-boc.boconcept.com",
        "environment" : "prod"
    },
	{
        "name" : "molio.dk",
        "environment" : "prod"
    }
]

var environments = 
[
    {
        "name" : "local",
        "color" : "green"
    },
    {
        "name" : "dev",
        "color" : "yellow"
    },
    {
        "name" : "test",
        "color" : "blue"
    },
    {
        "name" : "prod",
        "color" : "red"
    }
];

var border =
{
    "size" : 20,
    "opacity" : 0.5
}