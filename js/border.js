init();

function init() {
    for (i=0; i<sites.length; i++) {
        if (location.href.indexOf(sites[i].name) == -1)
            continue;

        color = getColorFromEnvironment(sites[i].environment);
        renderBorder(color, border.size, border.opacity);
        break;
    }
}

function getColorFromEnvironment(environment) {
    for (i=0; i<environments.length; i++) {
        if (environments[i].name == environment)
            return environments[i].color;
    }
    return "black";
}

function renderBorder(color, size, opacity) {
    var borderLeft = document.createElement('div');
    var borderRight = document.createElement('div');
    var borderTop = document.createElement('div');
    var borderBottom = document.createElement('div');

    borderLeft.className = 'borderLeft';
    borderRight.className = 'borderRight';
    borderTop.className = 'borderTop';
    borderBottom.className = 'borderBottom';

    // Color
    borderLeft.style.background = color;
    borderRight.style.background = color;
    borderTop.style.background = color;
    borderBottom.style.background = color;

    // Size
    borderLeft.style.width = size + 'px';
    borderRight.style.width = size + 'px';
    borderTop.style.height = size + 'px';
    borderBottom.style.height = size + 'px';

    borderLeft.style.top = size + 'px';
    borderLeft.style.bottom = size + 'px';
    borderRight.style.top = size + 'px';
    borderRight.style.bottom = size + 'px';

    // Opacity
    borderLeft.style.opacity = opacity;
    borderRight.style.opacity = opacity;
    borderTop.style.opacity = opacity;
    borderBottom.style.opacity = opacity;

    document.body.appendChild(borderLeft);
    document.body.appendChild(borderRight);
    document.body.appendChild(borderTop);
    document.body.appendChild(borderBottom);
}